package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CourseAnotherService;


@Controller
public class CourseAnotherController {
    @Autowired
    CourseAnotherService courseService;

    @GetMapping("courseWithAmountOfStudent/{studentEnrolled}")
    public ResponseEntity<?> getCourseWithAmountOfStudent(@PathVariable Integer studentEnrolled) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto((this.courseService.getCourseWhichStudentEnrolledMoreThan(studentEnrolled))));
    }
}
