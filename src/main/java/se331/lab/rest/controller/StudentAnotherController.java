package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.StudentAnotherService;

@Controller
public class StudentAnotherController {
    @Autowired
    StudentAnotherService studentService;

    @GetMapping("/studentByName/{name}")
    public ResponseEntity<?> getStudentByNameContains(@PathVariable String name) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(this.studentService.getStudentByNameContains(name)));
    }
    @GetMapping("/studentByAdvisorName/{name}")
    public ResponseEntity<?> getStudentByAdvisorName(@PathVariable String name) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(this.studentService.getStudentWhoseAdvisorNameIs(name)));
    }
}
